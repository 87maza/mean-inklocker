
var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('shirtlist', ['shirtlist']);
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/shirtlist', function (req, res) {
  console.log('I received a GET request');

  db.shirtlist.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/shirtlist', function (req, res) {
  console.log(req.body);
  db.shirtlist.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/shirtlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.shirtlist.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/shirtlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.shirtlist.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/shirtlist/:id', function (req, res) {
  var id = req.params.id;
  console.log(req.body.name);
  db.shirtlist.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {name: req.body.name, size: req.body.size, color: req.body.color, gender: req.body.gender}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(1337);
console.log("leet-port is 1337");



