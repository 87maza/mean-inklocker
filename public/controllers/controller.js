var myApp = angular.module('myApp', []);
myApp.controller('AppCtrl', ['$scope', '$http', function($scope, $http) {
    console.log("Hi from controller");


var refresh = function() {
  $http.get('/shirtlist').success(function(response) {
    console.log("GET request complete from refresh func");
    $scope.shirtlist = response;
    $scope.shirt = "";
  });
};

refresh();

$scope.addshirt = function() {
  console.log($scope.shirt);
  $http.post('/shirtlist', $scope.shirt).success(function(response) {
    console.log(response);
    refresh();
  });
};

$scope.remove = function(id) {
  console.log(id);
  $http.delete('/shirtlist/' + id).success(function(response) {
    refresh();
  });
};

$scope.edit = function(id) {
  console.log(id);
  $http.get('/shirtlist/' + id).success(function(response) {
    $scope.shirt = response;
  });
};  

$scope.update = function() {
  console.log($scope.shirt._id);
  $http.put('/shirtlist/' + $scope.shirt._id, $scope.shirt).success(function(response) {
    refresh();
  })
};

$scope.deselect = function() {
  $scope.shirt = "";
}

}]);﻿

